using ClientsApi.Models;

#pragma warning disable CS1591
namespace ClientsApi.Tests
{
	public static class TestHelper
	{
		public static Client createTestClient(int number)
		{
			return new Client()
			{
				firstname = "fn" + number,
				lastname = "ln" + number,
				address = "address" + number,
				phone = "123-456-789",
				email = number + "@email.com"
			};
		}
	}
}