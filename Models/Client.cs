using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ClientsApi.Models
{
	/// <summary>
	/// Data model for a client.
	/// </summary>
	public class Client
	{
		///<value>Record unique id</value>
		public int id { get; set; }

		///<value>Client first name</value>
		[Required]
		[StringLength(100, MinimumLength = 2)]
		public string firstname { get; set; }

		///<value>Client last name</value>
		[Required]
		[StringLength(100, MinimumLength = 2)]
		public string lastname { get; set; }

		///<value>Client address</value>
		[Required]
		[StringLength(1000)]
		public string address { get; set; }

		///<value>Client email</value>
		[Required]
		[EmailAddress]
		public string email { get; set; }

		///<value>Client phone number</value>
		[Required]
		[Phone]
		public string phone { get; set; }

		///<value>List of related client skills</value>
		public List<ClientSkill> skills { get; set; }
	}
}