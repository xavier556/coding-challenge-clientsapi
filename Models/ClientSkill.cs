using System.ComponentModel.DataAnnotations;

namespace ClientsApi.Models
{

	/// <summary>
	/// Define a relation between a client and a skill.
	/// </summary>
	public class ClientSkill
	{
		/// <value>Reference a Client id</value>
		[Required]
		public int clientId { get; set; }

		/// <value>Reference a Client id</value>
		[Required]
		public int skillId { get; set; }

		/// <value>Client navigation property</value>
		public Client client { get; set; }

		/// <value>Skill navigation property</value>
		public Skill skill { get; set; }
	}

}