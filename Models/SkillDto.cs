using System.ComponentModel.DataAnnotations;

namespace ClientsApi.Models.Dto
{
	/// <summary>
	/// Data transfer object for a skill.
	/// </summary>
	public class SkillDto
	{
		///<value>Skill record id</value>
		public int id { get; set; }

		///<value>Skill name</value>
		[Required]
		[StringLength(100, MinimumLength = 2)]
		public string name { get; set; }

		///<value>Skill level of expretise</value>
		[Required]
		[StringLength(20, MinimumLength = 2)]
		public string level { get; set; }
	}
}