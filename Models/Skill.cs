using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ClientsApi.Models
{
	/// <summary>
	/// Data model for a skill.
	/// </summary>
	public class Skill
	{
		///<value>Record unique id</value>
		[Required]
		public int id { get; set; }

		///<value>Skill name</value>
		[Required]
		[StringLength(100, MinimumLength = 2)]
		public string name { get; set; }

		///<value>Skill level of expretise</value>
		[Required]
		[StringLength(20, MinimumLength = 2)]
		public string level { get; set; }

		///<value>List of related client skills</value>
		public List<ClientSkill> clients { get; set; }
	}
}