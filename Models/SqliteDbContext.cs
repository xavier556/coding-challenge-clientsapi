using Microsoft.EntityFrameworkCore;
using ClientsApi.Models;

/// <summary>
/// Database context for sqlite.
/// </summary>
public class SqliteDbContext : DbContext
{
	/// <value>Clients</value>
	public DbSet<Client> Clients { get; set; }

	/// <value><see cref="Skills"/></value>
	public DbSet<Skill> Skills { get; set; }

	/// <value>Client skills</value>
	public DbSet<ClientSkill> ClientSkill { get; set; }

	///
	protected override void OnConfiguring(DbContextOptionsBuilder options) => options.UseSqlite("Data Source=ClientsApi.db");

	///
	protected override void OnModelCreating(ModelBuilder modelBuilder)
	{
		modelBuilder.Entity<ClientSkill>().HasKey(cs => new { cs.clientId, cs.skillId });
	}
}